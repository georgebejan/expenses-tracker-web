import React, {Component} from 'react';
import {Button} from 'react-bootstrap';
import axios from 'axios';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        }
    }

    handleClick(event) {
        const apiBaseUrl = "http://localhost:3001/";

        const payload = {
            "email": this.state.email,
            "password": this.state.password
        };

        axios.post(apiBaseUrl + 'authenticate', payload)
            .then(function (response) {
                console.log(response);
                if (response.status === 200) {
                    console.log("Login successfull");
                    console.log(response.data)
                }
                else if (response.status === 204) {
                    console.log("Username password do not match");
                    alert("username password do not match")
                }
                else {
                    console.log("Username does not exists");
                    alert("Username does not exist");
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12 text-center">
                        <div className="form-control">
                            <label>Email</label>
                            <input type="text"/>
                        </div>
                        {/*<TextField*/}
                        {/*label="Email"*/}
                        {/*margin="normal"*/}
                        {/*value={this.state.email}*/}
                        {/*onChange={event => this.setState({email: event.target.value})}*/}
                        {/*/>*/}
                        {/*<br/>*/}
                        {/*<TextField*/}
                        {/*type="password"*/}
                        {/*label="Password"*/}
                        {/*margin="normal"*/}
                        {/*value={this.state.password}*/}
                        {/*onChange={event => this.setState({password: event.target.value})}*/}
                        {/*/>*/}
                        {/*<br/>*/}
                        <Button bsStyle="success" bsSize="small" onClick={(event) => this.handleClick(event)}>
                            Submit
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;
